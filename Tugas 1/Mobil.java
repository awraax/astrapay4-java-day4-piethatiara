package Assignment;

public class Mobil {
    String merek1, merek2;
    String warna1, warna2;
    boolean status;

    void info1(String merek, String warna){
        this.merek1 = merek;
        this.warna1 = warna;
    }

    void info2(String merek, String warna){
        this.merek2 = merek;
        this.warna2 = warna;
    }

    void infoTampil(){
        System.out.println("Merek Mobil 1= "+ merek1);
        System.out.println("Warna Mobil 1= "+ warna1);
        System.out.println("Merek Mobil 2= "+ merek2);
        System.out.println("Warna Mobil 2= "+ warna2);
    }

    void berjalan(){
        status = true;
        System.out.println(merek1 + " warna " + warna1 + " sedang berjalan");
        System.out.println(merek2 + " warna " + warna2 + " sedang berjalan");
    }

    void berhenti(){
        status = false;
        System.out.println(merek1 + " warna " + warna1 + " sedang berhenti");
        System.out.println(merek2 + " warna " + warna2 + " sedang berhenti");
    }
    public static void main(String[] args) {
        Mobil infoMobil = new Mobil();

        infoMobil.info1("Toyota Camry", "Hitam");
        infoMobil.info2("Toyota Rush", "Putih");
        infoMobil.infoTampil();

        System.out.println(" ");
        infoMobil.berjalan();

        System.out.println(" ");
        infoMobil.berhenti();
    }
}
