package Assignment;

import java.util.Scanner;

public class Kalkulator {
    public static int penjumlahan (int a, int b){
        return a + b;
    }
    public static int pengurangan (int a, int b){
        return a - b;
    }
    public static int perkalian (int a, int b){
        return a * b;
    }
    public static int pembagian (int a, int b){
        return a / b;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number1, number2, pilih = 0, hasil;

        while (pilih != 5) {
            System.out.println("\n KALKULATOR \n");
            System.out.println("1. Pertambahan");
            System.out.println("2. Pengurangan");
            System.out.println("3. Perkalian");
            System.out.println("4. Pembagian");

            System.out.print("Input nomor = ");
            pilih = input.nextInt();
            
            if (pilih == 5) {
                break;
            } else if (pilih >= 6) {
                System.out.println("Pilihan tidak tersedia");
                continue;
            }

            System.out.print("Bilangan 1 = ");
            number1 = input.nextInt();
            System.out.print("Bilangan 2 = ");
            number2 = input.nextInt();
            switch (pilih) {
                case 1:
                    hasil = penjumlahan(number1, number2);
                    System.out.println("Pertambahan = " +hasil);
                    break;
                case 2:
                    hasil = pengurangan(number1, number2);
                    System.out.println("Pengurangan = " +hasil);
                    break;
                case 3:
                    hasil = perkalian(number1, number2);
                    System.out.println("Perkalian = " +hasil);
                    break;
                case 4:
                    hasil = pembagian(number1, number2);
                    System.out.println("Pembagian = " +hasil);
                    break;
            }
        }
        input.close();
    }
}
