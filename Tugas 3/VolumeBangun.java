package Assignment;

import java.util.Scanner;

public class VolumeBangun {

    public static int volBangun (int p, int l, int t){
        return p * l * t;
    }
    public static int volBangun (double phi, int jari){
        return (int) (4 * phi * (jari * jari * jari)/3);
    }
    public static int volBangun (double phi, int jari, int t){
        return (int) (phi * (jari * jari) * t);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilih = 0, hasil;

        while (true){
            System.out.println("\n MENU \n");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Volume Tabung");
            System.out.println("4. Exit");

            System.out.print("Input nomor = ");
            pilih = input.nextInt();

            if (pilih == 4){
                break;
            } else if (pilih > 4) {
                System.out.println("Pilihan tidak tersedia");
                continue;
            }

            switch (pilih){
                case 1:
                    System.out.print("Panjang = ");
                    int panjang = input.nextInt();
                    System.out.print("Lebar = ");
                    int lebar = input.nextInt();
                    System.out.print("Tinggi = ");
                    int tinggi = input.nextInt();

                    hasil = volBangun(panjang, lebar, tinggi);
                    System.out.println("Volume Balok = " +hasil); break;
                case 2:
                    System.out.print("Phi = ");
                    double phi = input.nextDouble();
                    System.out.print("Jari-Jari = ");
                    int jari = input.nextInt();

                    hasil = volBangun(phi, jari);
                    System.out.println("Volume Bola = "+ hasil); break;
                case 3:
                    System.out.print("Phi = ");
                    phi = input.nextDouble();
                    System.out.print("Jari-Jari = ");
                    jari = input.nextInt();
                    System.out.print("Tinggi = ");
                    tinggi = input.nextInt();

                    hasil = volBangun(phi, jari, tinggi);
                    System.out.println("Volume Tabung = "+ hasil);break;
            }
        }
        input.close();
    }
}
