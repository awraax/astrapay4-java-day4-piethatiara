package Assignment;

import java.util.Scanner;

public class Mahasiswa {
    String nama, jurusan;
    int fisika, kimia, biologi;

    Mahasiswa(){
        this.nama="Default";
        this.jurusan ="Default";
    }

    Mahasiswa(String nama, String jurusan){
        this.nama = nama;
        this.jurusan = jurusan;
    }

    Mahasiswa(String nama, String jurusan, int fisika, int kimia, int biologi){
        this.nama = nama;
        this.jurusan = jurusan;
        this.fisika =fisika;
        this.kimia = kimia;
        this.biologi = biologi;
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilih = 0;

        while (true){
            System.out.println("\n MENU \n");
            System.out.println("1. Konstruktur tanpa Parameter");
            System.out.println("2. Konstruktur dengan 2 Parameter");
            System.out.println("3. Konstruktur dengan 5 Parameter");
            System.out.println("4. Exit");

            Mahasiswa mahasiswa = new Mahasiswa();
            System.out.print("Input nomor = ");
            pilih = input.nextInt();

            if (pilih == 4){
                break;
            } else if (pilih > 4) {
                System.out.println("Pilihan tidak tersedia");
                continue;
            }

            switch (pilih){
                case 1:
                    System.out.println("Konstruktor tanpa Parameter"); break;
                case 2:
                    System.out.println("Konstruktor dengan 2 Parameter");
                    mahasiswa = new Mahasiswa("Steve", "IT");
                    break;
                case 3:
                    System.out.println("Konstruktor dengan 5 Parameter");
                    mahasiswa = new Mahasiswa("Steve", "IT", 7,8,9); break;
            }
            System.out.print(mahasiswa.nama + " ");
            System.out.print(mahasiswa.jurusan + "\n");
            System.out.print(mahasiswa.fisika + " ");
            System.out.print(mahasiswa.kimia + " ");
            System.out.print(mahasiswa.biologi);
        }
        input.close();
    }
}
