package Assignment;

import java.util.Scanner;

public class DataMahasiswa {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilih = 0;
        String data = "";

        while (true){
            System.out.println("\n MENU \n");
            System.out.println("1. Input Data Mahasiswa");
            System.out.println("2. Parsing dan Print Data");
            System.out.println("3. Exit");

            System.out.print("Input nomor = ");
            pilih = input.nextInt();

            if (pilih == 3){
                break;
            } else if (pilih > 3) {
                System.out.println("Pilihan tidak tersedia");
                continue;
            }

            switch (pilih){
                case 1:
                    System.out.println("Input Data Mahasiswa");
                    System.out.print("Input Nama = ");
                    String nama = input.next();
                    System.out.print("Input Nilai Fisika = ");
                    int nilaiFisika = input.nextInt();
                    System.out.print("Input Nilai Kimia = ");
                    int nilaiKimia = input.nextInt();
                    System.out.print("Input Nilai Biologi = ");
                    int nilaiBiologi = input.nextInt();
                    data = nama + "," + nilaiFisika + "," + nilaiKimia + "," + nilaiBiologi;
                    System.out.println(data);
                    break;
                case 2:
                    System.out.println("Parsing and print data");
                    String [] arrayData = data.split(",",4);
                    System.out.println(arrayData[0]);
                    for (int i = 1; i < arrayData.length; i++) {
                        System.out.print(arrayData[i] + " ");
                    }
                    System.out.println();
                    break;
            }
        }
        input.close();
    }
}
